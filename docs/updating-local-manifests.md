# Updating manifests from latest release

```bash
rm -rf manifests/* kibana/
helm repo add elastic https://helm.elastic.co
helm repo update
helm fetch elastic/kibana --untar
helm template kibana \
    --output-dir manifests \
    --namespace kibana \
    kibana/
rm -rf manifests/kibana/templates/test
cp -r manifests/kibana/templates/* manifests/
pushd manifests/kibana/templates/
for n in *; do printf '%s\n' "- $n"; done > ../../tmp-resources
cd ../../
cat <<EOF > kustomization.yml
resources:
$(cat tmp-resources)
EOF
popd
rm -rf manifests/kibana manifests/tmp-resources kibana
```