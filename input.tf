variable "kubeconfig" {
  type = string
  default = "~/.kube/config"
}

variable "namespace" {
  type = string
  default = "kibana"
}

variable "elasticsearch-host" {
  type = string
  default = "elasticsearch-master"
}

variable "elasticsearch-port" {
  type = string
  default = "9200"
}