data "kustomization_overlay" "kibana-prerequisites" {
  resources = [
    "${path.module}/pre-install-manifests",
  ]

  namespace = var.namespace
}

data "kustomization_overlay" "kibana" {
  resources = [
    "${path.module}/manifests",
  ]

  namespace = var.namespace

  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/template/spec/containers/0/env/0
        value:
          name: ELASTICSEARCH_HOSTS
          value: "http://${var.elasticsearch-host}:${var.elasticsearch-port}"
    EOF
    target = {
      kind = "Deployment"
      name = "kibana-kibana"
    }
  }
}

/**
 * When the Kustomization provider run through the manifests,
 * it cannot ensure that the namespace will be created first,
 * due to a ID limitation in the Terraform provider SDK.
 *
 * We therefor have a directory with prerequisites,
 * which we can install first,
 * and use that in a depends_on,
 * to ensure all prerequisites are met for our manifests.
 *
 * We also use a directory with a kustomization built in,
 * to ensure we can use the manifests without necessarily needing Terraform,
 * and could still use Kustomize directly on them.
 */
resource "kustomization_resource" "kibana-prerequisites" {
  for_each = data.kustomization_overlay.kibana-prerequisites.ids

  manifest = data.kustomization_overlay.kibana-prerequisites.manifests[each.value]
}

/**
 * Once all of the prerequisites exist,
 * we can continue to create all of the other manifests,
 * and let Kubernetes take care of the rest for us.
 */
resource "kustomization_resource" "kibana" {
  depends_on = [kustomization_resource.kibana-prerequisites]

  for_each = data.kustomization_overlay.kibana.ids

  manifest = data.kustomization_overlay.kibana.manifests[each.value]
}
